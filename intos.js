const expressApp = require('express');
const Transmission = require('transmission');
const multer = require('multer');
const cors = require('cors');
const mediainfo = require('./mediainfo');
const UploadWatcher = require('./watcher');
const { filenameToGlyphType, filesAddGlyphType, isMedia, getExt } = require('./fileToGlyph');

const uploadStorage = multer.memoryStorage();
const upload = multer({ storate: uploadStorage });
const trans = new Transmission({
    host: process.env.TRANSMISSION_HOST || 'localhost',
    port: Number(process.env.TRANSMISSION_PORT || 9091),
    username: process.env.TRANSMISSION_USER || 'admin',
    password: process.env.TRANSMISSION_PASSWORD
});
const app = expressApp();
app.use(expressApp.json());
const watcher = new UploadWatcher();


app.set('trust proxy', 'uniquelocal');

const validMatchers = [
    /::1/,
    /127\.\d{1,3}\.\d{1,3}\.\d{1,3}/,
    /10\.(8|10)\.\d{1,3}\.\d{1,3}/,
    /192\.168\.2\.\d{1,3}/,
];
app.use((req, res, next) => {
    if(validMatchers.reduce((a, b) => a || req.ip.match(b), false)) {
        res.header('Access-Control-Allow-Origin', '*');
        console.log(`[${req.method} ${req.path}] ${req.ip}`);
        next();
    } else {
        res.status(403).send('Solo se puede acceder desde la red de la Residencia');
        console.log(`[${req.method} ${req.path}] ${req.ip} unauthorized connection`);
    }
});

app.use('*', cors());

//Gets all the torrents and files/folders.
app.get('/torrents', (req, res) => {
    trans.get(function(err, arg) {
        if(err) {
            res.send({ error: err.result, code: 1 });
        } else {
            let torrents = [];
            for(let torrent of arg.torrents) {
                torrents.push({
                    addedDate: torrent.addedDate,
                    eta: torrent.eta,
                    id: torrent.id,
                    name: torrent.name,
                    totalSize: torrent.totalSize,
                    percentDone: torrent.percentDone,
                    recheckProgress: torrent.recheckProgress,
                    rateDownload: torrent.rateDownload,
                    rateUpload: torrent.rateUpload,
                    status: torrent.status,
                    type: filenameToGlyphType(torrent.name)
                });
            }

            for(let file of watcher.files) {
                torrents.push({
                    addedDate: file.addedDate,
                    eta: -1,
                    id: file.id,
                    name: file.name,
                    totalSize: file.totalSize,
                    status: 6,
                    uploaded: true,
                    type: file.type
                });
            }

            res.send({ results: torrents });
        }
    });
});

//Gets information about a torrent given its number (or id is da same)
app.get('/info_torrent/:num', function(req, res) {
    trans.get([Number(req.params.num)], function(err, arg) {
        if(err) {
            res.status(404).send({ error: err.result, code: 1 });
        } else if(arg.torrents.length === 0) {
            res.status(404).send({ error: 'Torrent id ' + req.params.num + ' not found', code: 2, id: req.params.num });
        } else {
            var torrent = arg.torrents[0];
            torrent = ({
                addedDate: torrent.addedDate,
                eta: torrent.eta,
                files: filterFiles(filesAddGlyphType(torrent.files), torrent.wanted),
                id: torrent.id,
                name: torrent.name,
                totalSize: torrent.totalSize,
                percentDone: torrent.percentDone,
                recheckProgress: torrent.recheckProgress,
                rateDownload: torrent.rateDownload,
                rateUpload: torrent.rateUpload,
                status: torrent.status,
                type: filenameToGlyphType(torrent.name)
            });

            res.send(torrent);
        }
    });
});

//Gets information about a file or folder from the uploaded folder given its number
app.get('/info_upload/:num', function(req, res) {
    var num = Number(req.params.num);
    if(watcher.files.length > num && num >= 0) {
        const obj = watcher.files[num];
        res.send(obj);
    } else {
        res.status(404).send({ error: 'Upload id ' + num + ' not found', code: 3, id: num });
    }
});

//Gets the mediainfo for a type (torrent or upload) of item, given its number (or id)
//and the file number
app.get('/mediainfo/:type/:id/:filenum', async function(req, res) {
    let num = Number(req.params.id);
    let filenum = Number(req.params.filenum);
    let type = req.params.type;
    if(type === 'torrent') {
        trans.get([num], async function(err, arg) {
            if(err) {
                res.status(404).send({ error: err.result, code: 1 });
            } else if(arg.torrents.length === 0) {
                res.status(404).send({ error: 'Torrent id ' + num + ' not found', code: 2, id: num });
            } else {
                const torrent = arg.torrents[0];
                let file = filterFiles(filesAddGlyphType(torrent.files), torrent.wanted).filter(f => f.id === filenum);
                if(file.length === 0) {
                    res.status(404).send({ error: 'Cannot find file with id ' + filenum + ' in torrent id ' + num, code: 3, id: num });
                } else {
                    try {
                        let a = await mediainfo(`/torrents/${file[0].name}`);
                        res.send(a);
                    } catch(e) {
                        res.status(500).send({ error: 'Cannot read mediainfo', code: 5 });
                    }
                }
            }
        });
    } else if(type === 'upload') {
        if(0 <= num && num < watcher.files.length) {
            let entry = watcher.files[num];
            try {
                res.send(await mediainfo(watcher._path + '/' + entry.files.filter(f => f.id === filenum)[0].name));
            } catch(e) {
                res.status(500).send({ error: 'Cannot read mediainfo', code: 5 });
            }
        } else {
            res.status(404).send({ error: 'Upload id ' + num + ' not found', code: 3, id: num });
        }
    } else {
        res.status(400).send({ what: 'is dis' });
    }
});

//Adds a torrent to the transmission daemon. Can be a magnet or a .torrent file
//A magnet must be sent as a json with { url: 'MAGNET_URL' } format
//A torrent file, must be the file in multipart format
app.post('/add_torrent', upload.single('file'), function(req, res) {
    let args = {};
    if(req.get("Content-Type").match(/application\/json(;charset=[A-Z\-0-9]+)?/i)) {
        if(!req.body.url) {
            res.status(400).send({ error: 'Empty body url', code: 4 }).end();
            return;
        }
        args.filename = req.body.url;
    } else {
        if(!req.file) {
            res.status(400).send({ error: 'No file uploaded', code: 5 });
            return;
        }
        args.metainfo = req.file.buffer.toString('base64');
    }

    trans.addTorrentDataSrc(args, {paused:false}, function(err, arg) {
        if(err) res.status(500).send({ error: err.result, code: 1 });
        else res.send(arg);
    });
});

//Downloads the file inside a torrent
app.get('/download_file/:id/:num', function(req, res) {
    const id = Number(req.params.id);
    const num = Number(req.params.num);

    trans.get([id], function(err, arg) {
        if(err || arg.torrents[0] === undefined) {
            res.header('Content-Type', 'text/html');
            res.status(404).send("Torrent not found<br><br>" + err);
        } else {
            const torrent = arg.torrents[0];
            const file = filterFiles(torrent.files, torrent.wanted).filter(f => f.id === num);
            if(file.length !== 0) {
                const path = torrent.downloadDir + '/' + file[0].name;
                res.redirect(`/intos/static_t/${file[0].name}`);
            } else {
                res.header('Content-Type', 'text/html');
                res.status(404).send("File from torrent " + id + " not found");
            }
        }
    });
});

//Downloads the file inside the upload's folder
app.get('/download_file2/:id/:num', function(req, res) {
    const id = Number(req.params.id);
    const num = Number(req.params.num);

    if(watcher.files[id] !== undefined) {
        const file = watcher.files[id].files.filter(f => f.id === num);
        if(file.length !== 0) {
            res.redirect(`/intos/static/${file[0].name}`);
        } else {
            res.header('Content-Type', 'text/html');
            res.status(404).send("File from uploaded folder " + id + " not found");
        }
    } else {
        res.header('Content-Type', 'text/html');
        res.status(404).send("Uploaded folder " + id + " not found");
    }
});

const P = ':80';
//Given a type (torrent or upload) and an id, returns a playlist with all the
//media files
app.get('/pls/:type/:id', function(req, res) {
    const type = req.params.type;
    const id = Number(req.params.id);

    if(type === 'torrent') {
        trans.get([id], function(err, arg) {
            if(err || arg.torrents[0] === undefined) {
                res.header('Content-Type', 'text/html');
                res.status(404).send("Torrent not found<br><br>" + err);
            } else {
                let arrPls = [];
                const files = filterFiles(arg.torrents[0].files, arg.torrents[0].wanted);
                for(let i = 0; i < files.length; i++) {
                    const file = files[i];
                    const ext = getExt(file.name);

                    if(isMedia(ext)) {
                        arrPls.push({
                            url: `http://${req.hostname}${P}${req.baseUrl}/api-intos/download_file/${id}/${i}`,
                            title: file.name
                        });
                    }
                }

                res.header('Content-Type', 'audio/x-scpls');
                res.header('Content-Disposition', `attachment; filename="${arg.torrents[0].name}.pls"`);
                res.send(objToPls(arrPls));
            }
        });
    } else if(type === 'upload') {
        if(watcher.files[id] !== undefined) {
            let arrPls = [];
            for(let i = 0; i < watcher.files[id].files.length; i++) {
                const file = watcher.files[id].files[i];
                const ext = getExt(file.name);

                if(isMedia(ext)) {
                    arrPls.push({
                        url: `http://${req.hostname}${P}${req.baseUrl}/api-intos/download_file2/${id}/${i}`,
                        title: file.name
                    });
                }
            }

            res.header('Content-Type', 'audio/x-scpls');
            res.header('Content-Disposition', `attachment; filename="${watcher.files[id].name}.pls"`);
            res.send(objToPls(arrPls));
        } else {
            res.header('Content-Type', 'text/html');
            res.status(404).send("Uploaded folder " + id + " not found");
        }
    } else {
        res.status(404).send('Type is not valid: ' + type);
    }
});

//Same as before, but only with a file
app.get('/pls/:type/:id/:num', function(req, res) {
    const type = req.params.type;
    const id = Number(req.params.id);
    const num = Number(req.params.num);

    if(type === 'torrent') {
        trans.get([id], function(err, arg) {
            if(err || arg.torrents[0] === undefined) {
                res.header('Content-Type', 'text/html');
                res.status(404).send("Torrent not found<br><br>" + err);
            } else {
                if(arg.torrents[0].files.length === 0) {
                    res.status(404).send('File not found for torrent ' + id);
                    return;
                }

                const files = filterFiles(arg.torrents[0].files, arg.torrents[0].wanted);
                res.header('Content-Type', 'audio/x-scpls');
                res.header('Content-Disposition', `attachment; filename="${files.filter(f => f.id === num)[0].name}.pls"`);
                res.send(objToPls([{
                    url: `http://${req.hostname}${P}${req.baseUrl}/api-intos/download_file/${id}/${num}`,
                    title: arg.torrents[0].files[num].name
                }]));
            }
        });
    } else if(type === 'upload') {
        if(watcher.files[id] !== undefined) {
            if(watcher.files[id].files[num] === undefined) {
                res.status(404).send('File not found for uploaded folder ' + id);
                return;
            }

            let filename = watcher.files[id].files[num].name.replace(watcher.files.filter(f => f.id === num)[0].name, '').substr(1);
            res.header('Content-Type', 'audio/x-scpls');
            res.header('Content-Disposition', `attachment; filename="${filename}.pls"`);
            res.send(objToPls([{
                url: `http://${req.hostname}${P}${req.baseUrl}/api-intos/download_file2/${id}/${num}`,
                title: watcher.files[id].files[num].name
            }]));
        } else {
            res.header('Content-Type', 'text/html');
            res.status(404).send("Uploaded folder " + id + " not found");
        }
    } else {
        res.status(404).send('Type is not valid: ' + type);
    }
});

/** Filter the torrent files for those that they were wanted to be downloaded */
var filterFiles = function(files, wanted) {
    let filteredFiles = [];
    for(let i in files) {
        if(!files.hasOwnProperty(i)) continue;
        if(wanted[i] === 1) {
            filteredFiles.push(files[i]);
        }
    }

    //                                  ...we must add an id
    return filteredFiles.map((f, id) => ({ ...f, id }));
};

/** Convert an array of { title: '', url: '' } into a PLS file */
var objToPls = function(arr) {
    let pls = '[playlist]\nNumberOfEntries=' + arr.length + '\n';

    for(let i = 0; i < arr.length; i++) {
        pls += 'File' + i + '=' + arr[i].url + '\n';
        if(arr[i].title) pls += 'Title' + i + '=' + arr[i].title + '\n';
    }

    return pls;
};


app.listen(8089);
