const { Buffer } = require('buffer');
const { spawn } = require('child_process');

/** From a mediainfo, gets all the objects of type (lowercase) */
const getOfType = (parsed, type) => parsed.media.track
        .filter(o => o['@type'].toLowerCase() === type)
        .sort((a, b) => Number(a['@typeorder'] || '0') - Number(b['@typeorder'] || '0'));

/** Given the JSON output of mediainfo, parses it to have an object with every
    type of tracks as key, and as a value, an array with these tracks. */
const parseOutput = (output) => {
    const parsed = JSON.parse(output);
    let obj = {};
    const types = new Set(parsed.media.track.map(o => o['@type'].toLowerCase()));
    types.forEach(type => {
        obj[type] = getOfType(parsed, type);
    });
    obj.general = obj.general[0];
    obj.video = obj.video || [];
    obj.audio = obj.audio || [];
    obj.text = obj.text || [];
    return obj;
};

/** Executes mediainfo for that file and returns a Promise. **/
const mediainfo = (path) => {
    return new Promise((accept, reject) => {
        const mi = spawn('mediainfo', [ path, '--Output=JSON' ]);
        let outputChunks = [];
        let errorChunks = [];
        mi.stdout.on('data', (data) => outputChunks.push(data));
        mi.stderr.on('data', (data) => errorChunks.push(data));
        mi.on('close', (code) => {
            const output = Buffer.concat(outputChunks).toString('latin1');
            if(code === 0) {
                accept(parseOutput(output));
            } else {
                reject({ code, output, error: Buffer.concat(errorChunks).toString('latin1') });
            }
        });
    });
};

/**
 * Executes mediainfo over a file and returns useful information for it.
 * @param {string} path - A path to a file to obtain the info.
 * @return {Promise} A promise with the information.
 **/
const usefulMediainfo = async (path) => {
    const object = await mediainfo(path);
    return {
        duration: Number(object.general['Duration']),
        videos: object.video.map(video => ({
            id: Number(video['ID']),
            num: Number(video['@typeorder'] || 1),
            codec: video['Format'],
            width: Number(video['Width'].replace(' pixels', '').replace(' ', '')),
            height: Number(video['Height'].replace(' pixels', '').replace(' ', '')),
            bitRate: Number(video['Bit rate'] || video['BitRate']),
            fps: Number(video['Frame rate'] || video['FrameRate']),
            name: video['Title'],
            hdr: video.colour_primaries && video.colour_primaries.indexOf('BT.2020') !== -1,
        })),
        audios: object.audio.map(audio => ({
            id: Number(audio['ID']),
            num: Number(audio['@typeorder'] || 1),
            codec: audio['Format'],
            bitRate: Number(audio['Bit rate'] || audio['BitRate']),
            sampleRate: Number(audio['Sampling rate'] || audio['SamplingRate']),
            bitDepth: Number((audio['Bit depth'] || audio['BitDepth'] || '16').replace(' bits', '')),
            channels: Number((audio['Channel(s)'] || audio['Channels'] || 'NaN').replace(/ channels?/, '')),
            language: audio['Language'],
            name: audio['Title'],
        })),
        subtitles: object.text.map(text => ({
            id: Number(text['ID']),
            num: Number(text['@typeorder'] || 1),
            language: text['Language'],
            title: text['Title'],
        })),
        chapters: object.menu.map(menu => Object.entries(menu.extra)
            .map(([key, value]) => [key.split('_').slice(1), value.split(':').slice(1).join(':')])
            .map(([time, value]) => [`${time[0]}:${time[1]}:${time[2]}.${time[3]}`, value])
            .reduce((obj, [key, value]) => ({...obj, [key]: value}), {})
        ),
    };
};

module.exports = usefulMediainfo;
