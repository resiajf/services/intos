# Servicio de compartición de archivos (backend) [aka intos]

The service watches for a upload's folder and connects to a Transmission daemon
client to offer a service of listing and download of files, and the ability to
download torrents. The server listens in the port `8089`.

## Configuration via environment variables

 - `TRANSMISSION_HOST`: The host name or IP of the transmission daemon. Defaults
 to `localhost`.
 - `TRANSMISSION_PORT`: The port of the transmission daemon. Defaults to `9091`.
 - `UPLOAD_PATH`: The path where to look for the uploaded files. Defaults to
 `/uploads`.

## Dockerfile

The image uses a node.js base with Debian 9 (aka stretch) and over it, install
`mediainfo` command.

The container can be configured with the upper configuration files. But the
upload's folder must be mounted into the path you select in `UPLOAD_PATH` or the
default `/uploads`.

## API

Here it is a detailed documentation of the exposed REST API. The authentication
is done via client's IP (only allowed inside local network).

### GET /torrents

Gets all the torrents in the transmission daemon and files/folders in the
upload's folder. A correct response has the following format:

```javascript
[
    { //For a torrent
        "id": <Number>,
        "addedDate": <Number>, //Unix timestamp
        "eta": <Number>,
        "name": "<String>",
        "totalSize": <Number>,
        "percertDone": [0..1],
        "recheckProgress": [0..1] | undefined,
        "rateDownload": <Number>,
        "rateUpload": <Number>,
        "status": 0 | 1 | 2 | 3 | 4 | 5 | 6,
        "type": "<String>"
    },
    ...
    { //For a file/folder
        "id": <Number>,
        "addedDate": <Number>, //Unix timestamp
        "eta": -1,
        "name": "<String>",
        "totalSize": <Number>,
        "status": 6, //Means completed in transmission
        "uploaded": true, //Distinction between torrent and uploads
        "type": "<String>"
    },
    ...
]
```

**Errors**: 1

### GET /info_torrent/:num

 - `:num` Number: The torrent id

Gets full information of the torrent (the same as above, but with the files too)
in the following format:

```javascript
{
    "id": <Number>,
    "addedDate": <Number>, //Unix timestamp
    "eta": <Number>,
    "name": "<String>",
    "totalSize": <Number>,
    "percertDone": [0..1],
    "recheckProgress": [0..1] | undefined,
    "rateDownload": <Number>,
    "rateUpload": <Number>,
    "status": 0 | 1 | 2 | 3 | 4 | 5 | 6,
    "type": "<String>",
    "files": [
        {
            "bytesCompleted": <Number>,
            "length": <Number>,
            "name":"<String>",
            "type":"<String>"
        },
        ...
    ]
}
```

**Errors**: 1, 2

### GET /info_upload/:num

 - `:num` Number: The file/folder id

Gets full information of the file/folder (same as in info_torrent), with the
following format:

```javascript
{
    "id": <Number>,
    "addedDate": <Number>, //Unix timestamp
    "eta": -1,
    "files":[
        {
            "length": <Number>,
            "name":"<String>",
            "type":"<String>"
        },
        ...
    ],
    "name": "<String>",
    "totalSize": <Number>,
    "status": 6,
    "uploaded": true,
    "type": "<String>"
}
```

**Errors**: 3

### GET /mediainfo/:type/:id/:filenum

 - `:type` 'torrent' | 'upload': Whether check for a torrent or upload
 - `:id` Number: the ID of the torrent or file/folder
 - `:filenum` Number: the ID of the file (from `"files"` array)

Gets a small output of mediainfo command for the selected file. The output has
the following format:

```javascript
{
    "duration": <Number>, //in seconds
    "videos": [
        {
            "id": <Number>,
            "num": <Number>, //indicates the position of the track inside video tracks
            "codec": "<String>",
            "width": <Number>,
            "height": <Number>,
            "bitrate": <Number>,
            "fps": <Number>,
            "name": "<String>" | null
        },
        ...
    ],
    "audios": [
        {
            "id": <Number>,
            "num": <Number>, //indicates the position of the track inside audio tracks
            "codec": "<String>",
            "bitrate": <Number>,
            "sampleRate": <Number>, //in Hz
            "bitDepth": <Number>,
            "channels": <Number>,
            "language": "<String>", //short version
            "name": "<String>" | null
        },
        ...
    ],
    "subtitles": [
        {
            "id": <Number>,
            "num": <Number>, //indicates the position of the track inside the text tracks
            "language": "<String>",
            "title": "<String>" | null
        },
        ...
    ]
}
```

**Errors**: 1, 2, 3, 5

### POST /add_torrent

The body can be a magnet link or a `.torrent` file.

For a magnet link, the client must sent a JSON with the following format:

```javascript
{
    "url": "magnet:..."
}
```

For a torrent file, the client must send the file in the body.

If everything goes well, the result is unspecified (but is a good response so).

**Errors**: 1, 4, 5

### GET /download_file/:id/:num

 - `:id` Number: The torrent's ID
 - `:num` Number: The file's ID (from an item of `"files"` array)

Downloads the selected file from a torrent. Nothing more, nothing less.

**Errors**: Custom: Shows an HTML with the error as it is used in links directly.

### GET /download_file2/:id/:num

 - `:id` Number: The file/folder's ID
 - `:num` Number: The file's ID (from an item of `"files"` array)

Same of before, downloads the selected file from (in this case) a file/folder of
the upload's folder.

**Errors**: Custom: Shows an HTML with the error as it is used in links directly.

### GET /pls/:type/:id

 - `:type` 'torrent' | 'upload': Whether check for a torrent or upload
 - `:id` Number: The torrent of file/folder ID

Downloads a PLS playlist with all the media files found in the torrent or folder.

**Errors**: Custom: Shows an HTML with the error as it is used in links directly.

### GET /pls/:type/:id/:num

 - `:type` 'torrent' | 'upload': Whether check for a torrent or upload
 - `:id` Number: The torrent of file/folder ID
 - `:num` Number: The file's ID (from an item of the `"files"` array)

Downloads a PLS playlist with only the selected file inside.

**Errors**: Custom: Shows an HTML with the error as it is used in links directly.

### Errors

 - 1: _There was a problem with transmission daemon_
 - 2: _Cannot find torrent with the given ID_
 - 3: _Cannot find a file/folder (upload) with the given ID_
 - 4: _Invalid add torrent request: empty body_
 - 5: _There was an error with mediainfo_

The format is always:

```json
{
    "error": "Description of the error",
    "code": <Number>
}
```

## Running tests

There is some tests inside the project. Tests are rune with [mocha][1] or
[instanbul][2] if code coverage is wanted. Mocking is done with [chai][3] and
for modules, [rewiremock][4].

To run them execute `npm test` or `yarn test`. The coverage is done with
`npm run cov` or `yarn run cov`.


  [1]: https://mochajs.org
  [2]: https://istanbul.js.org
  [3]: http://www.chaijs.com
  [4]: https://github.com/theKashey/rewiremock
