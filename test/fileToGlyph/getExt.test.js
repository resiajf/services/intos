const expect = require('chai').expect;
const { getExt } = require('../../fileToGlyph');

describe('fileToGlyph.js', () => {
    describe('getExt()', () => {
        it('should return the extension of a file', () => {
            const filename = 'a simple file.js';

            const result = getExt(filename);

            expect(result).to.be.equal('js');
        });

        it('should return the extension of a file with a filename with more than one dot', () => {
            const filename = 'a.simple.test.js';

            const result = getExt(filename);

            expect(result).to.be.equal('js');
        });
    });
});