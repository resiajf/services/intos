const expect = require('chai').expect;
const { isMedia } = require('../../fileToGlyph');

describe('fileToGlyph.js', () => {
    describe('isMedia()', () => {
        it('should return true if it is inside the list', () => {
            const ext = 'mp3';

            const result = isMedia(ext);

            expect(result).to.be.true;
        });

        it('should return false if it is not inside the list', () => {
            const ext = 'cpp';

            const result = isMedia(ext);

            expect(result).to.be.false;
        });
    });
});
