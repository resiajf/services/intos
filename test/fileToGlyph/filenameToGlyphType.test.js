const expect = require('chai').expect;
const { filenameToGlyphType } = require('../../fileToGlyph');

describe('fileToGlyph.js', () => {
    describe('filenameToGlyphType()', () => {
        it('should return a type for a valid filename', () => {
            const filename = 'a.c';

            const result = filenameToGlyphType(filename);

            expect(result).to.be.equal('file-code-o');
        });

        it('should return "file-o" for an unknown filename', () => {
            const filename = 'a.rust';

            const result = filenameToGlyphType(filename);

            expect(result).to.be.equal('file-o');
        });

        it('should return "file-o" for an invalid filename', () => {
            const filename = 'a';

            const result = filenameToGlyphType(filename);

            expect(result).to.be.equal('file-o');
        });
    });
});
