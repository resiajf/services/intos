const expect = require('chai').expect;
const { filesAddGlyphType, filenameToGlyphType } = require('../../fileToGlyph');

describe('fileToGlyph.js', () => {
    describe('filesAddGlyphType()', () => {
        it('should add type attribute with a valid glyph type', () => {
            const files = ['a.mp3', 'a.m4a', 'a.mkv', 'a.c', 'd.c'].map(name => ({ name }));

            const result = filesAddGlyphType(files);

            result.forEach(res => expect(res.type).to.be.equal(filenameToGlyphType(res.name)));
        });

        it('should return a type for known file type', () => {
            const file = [ { name: 'a.mp3' } ];

            const result = filesAddGlyphType(file);

            expect(result[0].type).to.be.not.equal('file-o');
        });

        it('should return "file-o" for unknown file type', () => {
            const file = [ { name: 'a.rust' } ];

            const result = filesAddGlyphType(file);

            expect(result[0].type).to.be.equal('file-o');
        });
    });
});
