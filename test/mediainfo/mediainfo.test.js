const expect = require('chai').expect;
const EventEmitter = require('events');
const { createReadStream } = require('fs');
const rewiremock = require('rewiremock').default;

class ProcessStub extends EventEmitter {

    constructor(fail) {
        super();
        this.stdout = createReadStream('./test/mediainfo/mediainfo.ok.txt');
        this.stderr = createReadStream('./test/mediainfo/mediainfo.error.txt');
        this.stdout.on('end', () => this._onEnd(fail));
        this.stderr.on('end', () => this._onEnd(fail));
        this._endCount = 0;
    }

    _onEnd(fail) {
        this._endCount++;
        if(this._endCount == 2) {
            this.emit('close', fail ? 1 : 0);
        }
    }

}

rewiremock('child_process')
    .with({
        spawn: (_, args) => new ProcessStub(args[0] !== 'ok')
    });

describe('mediainfo.js', () => {
    beforeEach(() => rewiremock.enable());
    afterEach(() => rewiremock.disable());
    describe('usefulMediainfo', () => {
        it('should return useful info from a valid file', async () => {
            const mediainfo = require('../../mediainfo'); //For the mock to work
            const file = 'ok';

            const result = await mediainfo(file);

            expect(result).to.deep.equal({
                duration: 6302.038,
                videos: [
                    { id: 1,
                    num: 1,
                    codec: 'HEVC',
                    width: 3840,
                    height: 1608,
                    bitrate: 11973298,
                    fps: 23.976,
                    name: undefined } ],
                audios: [
                    { id: 2,
                       num: 1,
                       codec: 'AAC',
                       bitrate: 208871,
                       sampleRate: 48000,
                       bitDepth: 16,
                       channels: 6,
                       language: 'es',
                       name: 'Español' },
                     { id: 3,
                       num: 2,
                       codec: 'AAC',
                       bitrate: 208581,
                       sampleRate: 48000,
                       bitDepth: 16,
                       channels: 6,
                       language: 'en',
                       name: 'English' } ],
                subtitles:
                   [ { id: 4, num: 1, language: 'es', title: 'Español Forzados' },
                     { id: 5, num: 2, language: 'es', title: 'Español' },
                     { id: 6, num: 3, language: 'en', title: 'English' },
                     { id: 7, num: 4, language: 'en', title: 'English SDH' } ]
            });
        });

        it('should throw if invalid file', async () => {
            const mediainfo = require('../../mediainfo'); //For the mock to work
            try {
                await mediainfo('fail');
            } catch(e) {
                return;
            }
            expect().to.be.equal('Must throw');
        })
    });
});
