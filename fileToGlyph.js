/**
 * Returns the extension of a file. Fails if the file has no extension.
 * @param {string} filename - A file name with an extension.
 * @return {string} The extension of the file
 **/
const getExt = (filename) => filename.substring(filename.lastIndexOf('.') + 1);

/**
 * A not-very-accurated check for a file to see if it is an audio or video file.
 * @param {string} ext - Extension of the file
 * @return {boolean} True if it is a media file, false otherwise
 **/
const isMedia = (ext) => !!ext.match(/^(mp3)?(mp4)?(m4a)?(ogg)?(flac)?(alac)?(wav)?(wv)?(opus)?(avi)?(mkv)?$/i);

/**
 * Converts a file name into a FontAwesome class. If the file's extension is
 * unknown, will return `file-o`.
 * @param {string} filename - A file name
 * @return {string} The FontAwesome 4.7 class.
 **/
const filenameToGlyphType = (filename) => {
    const assoc = {
        mp3: 'audio',
        m4a: 'audio',
        mp4: 'video',
        ogg: 'audio',
        ogv: 'video',
        oga: 'audio',
        flac: 'audio',
        wv: 'audio',
        opus: 'audio',
        mkv: 'video',
        avi: 'video',
        zip: 'archive',
        rar: 'archive',
        '7z': 'archive',
        gz: 'archive',
        xz: 'archive',
        txt: 'text',
        nfo: 'text',
        pdf: 'word',
        jpg: 'image',
        jpeg: 'image',
        png: 'image',
        gif: 'image',
        webp: 'image',
        webm: 'video',
        htm: 'code',
        html: 'code',
        css: 'code',
        js: 'code',
        c: 'code',
        cpp: 'code',
        h: 'code',
        hpp: 'code',
        java: 'code',
        m3u: 'audio',
        pls: 'audio'
    };
    let ext = '';
    if(filename.lastIndexOf('.') != -1) {
        ext = getExt(filename);
    }
    return assoc[ext] !== undefined ? 'file-' + assoc[ext] + '-o' : 'file-o';
};

/**
 * Adds the FontAwesome's class on every file in the array. Uses
 * {@link filenameToGlyphType} for do that.
 * @param {array} files - An array of files (objects).
 * @return {array} the same array, but with `type` attribute added.
 **/
const filesAddGlyphType = (files) => {
    return files.map(file => ({...file, type: filenameToGlyphType(file.name) }));
};

module.exports = {

    filenameToGlyphType,
    filesAddGlyphType,
    isMedia,
    getExt,

};