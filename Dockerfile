FROM node:8-stretch

RUN mkdir /app
COPY package.json /app/
WORKDIR /app
RUN yarn && \
    apt update ; \
    apt install apt-transport-https && \
    wget https://mediaarea.net/repo/deb/repo-mediaarea_1.0-6_all.deb && \
    dpkg -i repo-mediaarea_1.0-6_all.deb && \
    apt update ; \
    apt install -y mediainfo && \
    rm repo-mediaarea_1.0-6_all.deb && \
    rm -r /var/lib/apt && \
    rm -r /var/cache/apt
COPY package.json mediainfo.js intos.js watcher.js fileToGlyph.js /app/

CMD node intos.js
EXPOSE 8089