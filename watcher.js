const fs = require('fs');
const chokidar = require('chokidar');
const { filenameToGlyphType } = require('./fileToGlyph');

const UPLOAD_PATH = process.env.UPLOAD_PATH || '/uploads';

/** Watches for changes in the upload folder, and holds the files list. */
class UploadWatcher {

    /**
     * Instantiates the object, fills the file list and starts listening.
     * @constructor
     * @param {string} uploadPath - The path to the upload folder.
     **/
    constructor(uploadPath = UPLOAD_PATH) {
        this._path = uploadPath;
        if(this._path[this._path.length - 1] === '/') {
            this._path = this._path.substr(0, this._path.length - 1);
        }
        //this._fillList();
        this._startListeningForChanges();
        //this._interval = setInterval(() => this._fillList, 5 * 60 * 1000);
    }

    /**
     * Getter to obtain the list of files.
     * @return {array} The list of files and folders.
     **/
    get files() {
        return this._files;
    }

    /**
     * Closes all the watch stuff.
     **/
    close() {
        //clearInterval(this._interval);
        this._watcher.close();
    }

    /** Fills the list of files and folders */
    _fillList() {
        const torrents = [];
        fs.readdir(this._path, (err, files) => {
            if(!err) {
                let num = 0;
                for(let file of files) {
                    if(file[0] == '.') continue; //Eliminar archivos ocultos
                    const stat = fs.statSync(`${this._path}/${file}`);

                    if(stat.isFile()) {
                        torrents.push(UploadWatcher._fileObject(file, stat, num));
                    } else if(stat.isDirectory()) {
                        let obj = UploadWatcher._fileObject(file, stat, num);
                        const dirObj = this._dirObject(file);
                        obj.files = dirObj.files;
                        obj.totalSize = dirObj.totalSize;
                        obj.type = 'folder-open-o';
                        torrents.push(obj);
                    }
                    num++;
                }
            } else {
                console.error(err);
            }

            this._files = torrents;
        });
    }

    /** Given a file, its id and the stat struct, fills the file's object */
    static _fileObject(file, stat, i) {
        return {
            addedDate: (stat.ctime || stat.mtime).getTime() / 1000,
            eta: -1,
            files: [{
                length: stat.size,
                name: file,
                type: filenameToGlyphType(file),
                id: 0
            }],
            id: i,
            name: file,
            totalSize: stat.size,
            status: 6,
            uploaded: true,
            type: filenameToGlyphType(file)
        };
    }

    /** Iterates over a directory to get all the files inside it. */
    _dirObject(dir) {
        let dir_files = [], totalSize = 0;
        let files_in_dir = fs.readdirSync(`${this._path}/${dir}`);

        for(var fileInDir of files_in_dir) {
            if(fileInDir[0] == '.') continue; //Eliminar archivos ocultos
            const stat = fs.lstatSync(`${this._path}/${dir}/${fileInDir}`);

            if(stat.isFile()) {
                dir_files.push({
                    length: stat.size,
                    name: `${dir}/${fileInDir}`,
                    type: filenameToGlyphType(fileInDir)
                });
                totalSize += stat.size;
            } else if(stat.isDirectory() && !stat.isSymbolicLink()) {
                const obj = this._dirObject(`${dir}/${fileInDir}`);
                totalSize += obj.totalSize;
                dir_files = dir_files.concat(obj.files);
            }
        }

        return {
            totalSize,
            files: dir_files.map((f, id) => ({ ...f, id })) //Adds the id
        };
    }

    _startListeningForChanges() {
        this._files = [];
        this._watcher = chokidar.watch(this._path, { ignored: '**/.*' });
        this._watcher
            .on('add', path => this._fileAdded(path))
            .on('addDir', path => this._folderAdded(path))
            .on('unlink', path => this._fileRemoved(path))
            .on('unlinkDir', path => this._folderRemoved(path))
            .on('changed', path => this._fileChanged(path));
    }

    _watcherCommon(path) {
        const relativePath = path.substr(this._path.length + 1);
        const dir = relativePath.replace(/\/.+/, '');
        const name = relativePath.replace(/^[^/]+\/?/, '');
        return {
            relativePath, dir, name,
            dirItem: this._files.filter(f => f.name === dir),
        };
    }

    _fileAdded(path) {
        const { relativePath, dir, name, dirItem } = this._watcherCommon(path);
        const stat = fs.lstatSync(path);
        if(dirItem.length === 0) {
            this._files.push(UploadWatcher._fileObject(relativePath, stat, this._files.length));
            this._log(`Added file "${dir}"`);
        } else {
            dirItem[0].totalSize += stat.size;
            dirItem[0].files.push({
                length: stat.size,
                name: relativePath,
                type: filenameToGlyphType(name),
            });
            this._log(`Added file "${name}" inside folder "${dir}"`);
        }
    }

    _folderAdded(path) {
        const { relativePath, dir, name, dirItem } = this._watcherCommon(path);
        const stat = fs.lstatSync(path);
        if(dir !== '') {
            if(dirItem.length === 0) {
                let obj = UploadWatcher._fileObject(relativePath, stat, this._files.length);
                //const dirObj = this._dirObject(relativePath);
                obj.files = [];//dirObj.files;
                obj.totalSize = 0;//dirObj.totalSize;
                obj.type = 'folder-open-o';
                this._files.push(obj);
                this._log(`Added folder "${dir}"`);
            } else {
                this._log(`New folder "${name}" inside folder "${dir}", doing nothing`);
            }
        }
    }

    _fileRemoved(path) {
        const { relativePath, dir, name, dirItem } = this._watcherCommon(path);
        if(name === '') {
            this._files = this._files.filter(f => f.name !== dir).map((f, id) => ({ ...f, id }));
            this._log(`Removed file "${dir}"`);
        } else {
            const fileItem = dirItem[0].files.filter(f => f.name === name)[0];
            dirItem[0].totalSize -= fileItem.length;
            dirItem[0].files = dirItem[0].filter(f => f.name !== name).map((f, id) => ({ ...f, id }));
            this._log(`Removed file "${name}" inside folder "${dir}"`);
        }
    }

    _folderRemoved(path) {
        const { relativePath, dir, name, dirItem } = this._watcherCommon(path);
        if(name === '') {
            this._files = this._files.filter(f => f.name !== dir).map((f, id) => ({ ...f, id }));
            this._log(`Removed folder "${dir}"`);
        } else {
            this._log(`Removed folder "${name}" inside folder "${dir}"`);
            const filesSizeItem = dirItem[0].files.filter(f => f.name.startsWith(name)).reduce((l, f) => l + f.length);
            dirItem[0].totalSize -= filesSizeItem;
            dirItem[0].files = dirItem[0].files.filter(f => !f.name.startsWith(name)).map((f, id) => ({ ...f, id }));
        }
    }

    _changed(path) {
        const { relativePath, dir, name, dirItem } = this._watcherCommon(path);
        const stat = fs.lstatSync(path);
        if(name === '') {
            if(stat.isFile()) {
                dirItem.length = stat.size;
                this._log(`Changed file "${dir}"`);
            }
        } else {
            if(stat.isFile()) {
                const fileItem = dirItem[0].files.filter(f => f.name === name)[0];
                fileItem.length = stat.size;
                this._log(`Changed file "${name}" in "${dir}"`);
            }
        }
    }

    _log(msg, ...args) {
        console.log(`[UploadWatcher] ${msg}`, ...args);
    }

}

module.exports = UploadWatcher;
